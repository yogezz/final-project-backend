package com.TaskManagement.jwt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JwtTaskManagementApplication {

    public static void main(String[] args) {
        SpringApplication.run(JwtTaskManagementApplication.class, args);
    }

}
