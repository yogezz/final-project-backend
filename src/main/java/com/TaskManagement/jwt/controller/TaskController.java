package com.TaskManagement.jwt.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.TaskManagement.jwt.dto.TaskDto;
import com.TaskManagement.jwt.entity.Task;
import com.TaskManagement.jwt.entity.User;
import com.TaskManagement.jwt.service.TaskService;

@RestController

public class TaskController {
	
	@Autowired
	private TaskService taskService;
	
	@PostMapping("/createTask")
	public TaskDto createTask(@RequestBody TaskDto taskDto) {
		return taskService.createTask(taskDto);
	}
	
	@GetMapping("/find/{id}")
	public Task find(@PathVariable("id") long taskId ) {
		return taskService.find(taskId);
	}
	
	@GetMapping("/getAllTask")
	public List<TaskDto>   getAllTask() {
		   return taskService.getAllTask();
	   }
	
	@DeleteMapping("deleteTask/{id}")
	public void deleteTask(@PathVariable Long id) {
    	taskService.deleteTask(id);
    }
	
	@PutMapping("updateTask/{id}")
	public void updateTask(@PathVariable Long id,@RequestBody TaskDto taskDto) {
		
    	 taskService.updateTask(id,taskDto);
    }
	
	@GetMapping("/getAllTaskByUserId/{id}")
	public List<TaskDto>   getAllTaskById(@PathVariable Long id) {
		   return taskService.getAllTaskByUserId(id);
	   }
	
	@GetMapping("/filterTask")
	public List <TaskDto> filterTask(@RequestParam (value="key")String key,@RequestParam (value="value")String value){
		List<TaskDto> data= taskService.filterTask(key, value);
		return data;
	}

}
