package com.TaskManagement.jwt.dao;




import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.TaskManagement.jwt.entity.Role;
import com.TaskManagement.jwt.entity.User;

@Repository
public interface RoleDao extends JpaRepository<Role, Long> {

	

}
