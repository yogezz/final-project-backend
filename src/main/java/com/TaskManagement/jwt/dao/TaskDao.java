package com.TaskManagement.jwt.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.TaskManagement.jwt.entity.Task;
import com.TaskManagement.jwt.entity.User;

public interface TaskDao extends JpaRepository<Task, Long> {

	List<Task> findByAssign_UserName(String userName);

	


	
	
}
