package com.TaskManagement.jwt.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.TaskManagement.jwt.entity.User;


@Repository
public interface UserDao extends JpaRepository<User, Long> {

	User findOneByUserNameContains(String userName);

	User findByUserName(String userName);


	

	
	
	
}
