package com.TaskManagement.jwt.dto;

import java.util.Date;

import com.TaskManagement.jwt.entity.User;

public class TaskDto {

	@Override
	public String toString() {
		return "TaskDto [taskId=" + taskId + ", taskDescription=" + taskDescription + ", taskNumber=" + taskNumber
				+ ", assign=" + assign + ", startingDate=" + startingDate + ", estimateDate=" + estimateDate
				+ ", taskStatus=" + taskStatus + ", sprint=" + sprint + ", getTaskId()=" + getTaskId()
				+ ", getTaskDescription()=" + getTaskDescription() + ", getTaskNumber()=" + getTaskNumber()
				+ ", getAssign()=" + getAssign() + ", getStartingDate()=" + getStartingDate() + ", getEstimateDate()="
				+ getEstimateDate() + ", getTaskStatus()=" + getTaskStatus() + ", getSprint()=" + getSprint()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString()
				+ "]";
	}

	private Long taskId;
	private String taskDescription;
	private int taskNumber;
	private String assign;
	private String startingDate;
	
	private String estimateDate;
	
	private String taskStatus;
	
	private int sprint;

	public TaskDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TaskDto(Long taskId, String taskDescription, int taskNumber, String assign, String startingDate,
			String estimateDate, String taskStatus, int sprint) {
		super();
		this.taskId = taskId;
		this.taskDescription = taskDescription;
		this.taskNumber = taskNumber;
		this.assign = assign;
		this.startingDate = startingDate;
		this.estimateDate = estimateDate;
		this.taskStatus = taskStatus;
		this.sprint = sprint;
	}


	

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getTaskDescription() {
		return taskDescription;
	}

	public void setTaskDescription(String taskDescription) {
		this.taskDescription = taskDescription;
	}

	public int getTaskNumber() {
		return taskNumber;
	}

	public void setTaskNumber(int taskNumber) {
		this.taskNumber = taskNumber;
	}

	public String getAssign() {
		return assign;
	}

	public void setAssign(String assign) {
		this.assign = assign;
	}

	public String getStartingDate() {
		return startingDate;
	}

	public void setStartingDate(String startingDate) {
		this.startingDate = startingDate;
	}

	public String getEstimateDate() {
		return estimateDate;
	}

	public void setEstimateDate(String estimateDate) {
		this.estimateDate = estimateDate;
	}

	public String getTaskStatus() {
		return taskStatus;
	}

	public void setTaskStatus(String taskStatus) {
		this.taskStatus = taskStatus;
	}

	public int getSprint() {
		return sprint;
	}

	public void setSprint(int sprint) {
		this.sprint = sprint;
	}

	
	
}
