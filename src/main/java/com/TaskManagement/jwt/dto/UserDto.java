package com.TaskManagement.jwt.dto;

import java.util.Set;

import com.TaskManagement.jwt.entity.Role;

public class UserDto {
	
	private Long userId;
    private String userName;
    private String userFirstName;
    private String userLastName;
    private String userPassword;
    private Long role;
    private String userEmail;
    
	
	
	
	
	public UserDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserDto(Long userId, String userName, String userFirstName, String userLastName, String userPassword,
			Long role,String userEmail) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.userFirstName = userFirstName;
		this.userLastName = userLastName;
		this.userPassword = userPassword;
		this.role = role;
		this.userEmail=userEmail;
	}

	
	

	public UserDto(Long userId2, String userName2, String userFirstName2, String userLastName2, String userPassword2,
			Set<Role> role2,String userEmail) {
		// TODO Auto-generated constructor stub
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserFirstName() {
		return userFirstName;
	}
	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}
	public String getUserLastName() {
		return userLastName;
	}
	public void setUserLastName(String userLastName) {
		this.userLastName = userLastName;
	}
	public String getUserPassword() {
		return userPassword;
	}
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}
	public Long getRole() {
		return role;
	}
	public void setRole(Long role) {
		this.role = role;
	}

}
