package com.TaskManagement.jwt.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToOne;

@Entity
public class Task {
	public Task() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long taskId;
	
	private String taskDescription;
	
	private int taskNumber;
	
	@OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name="fk_user_id")
	private User assign;
	
	private String startingDate;
	
	private String estimateDate;
	
	private String taskStatus;
	
	private int sprint;
	
	public Task(Long taskId, String taskDescription, int taskNumber, User assign, String startingDate, String estimateDate,
			String taskStatus, int sprint) {
		super();
		this.taskId = taskId;
		this.taskDescription = taskDescription;
		this.taskNumber = taskNumber;
		this.assign = assign;
		this.startingDate = startingDate;
		this.estimateDate = estimateDate;
		this.taskStatus = taskStatus;
		this.sprint = sprint;
	}

	



	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getTaskDescription() {
		return taskDescription;
	}

	public void setTaskDescription(String taskDescription) {
		this.taskDescription = taskDescription;
	}

	public int getTaskNumber() {
		return taskNumber;
	}

	public void setTaskNumber(int taskNumber) {
		this.taskNumber = taskNumber;
	}

	public User getAssign() {
		return assign;
	}

	public void setAssign(User assign) {
		this.assign = assign;
	}

	public String getStartingDate() {
		return startingDate;
	}

	public void setStartingDate(String startingDate) {
		this.startingDate = startingDate;
	}

	public String getEstimateDate() {
		return estimateDate;
	}

	public void setEstimateDate(String estimateDate) {
		this.estimateDate = estimateDate;
	}

	public String getTaskStatus() {
		return taskStatus;
	}

	public void setTaskStatus(String taskStatus) {
		this.taskStatus = taskStatus;
	}

	public int getSprint() {
		return sprint;
	}

	public void setSprint(int sprint) {
		this.sprint = sprint;
	}

	

}
