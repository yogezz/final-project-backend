package com.TaskManagement.jwt.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.TaskManagement.jwt.dao.TaskDao;
import com.TaskManagement.jwt.dao.UserDao;
import com.TaskManagement.jwt.dto.TaskDto;
import com.TaskManagement.jwt.dto.UserDto;
import com.TaskManagement.jwt.entity.Role;
import com.TaskManagement.jwt.entity.Task;
import com.TaskManagement.jwt.entity.User;



@Service
public class TaskService {
	
	@Autowired
	private TaskDao taskDao;
	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	JavaMailSender mailSender;
	
	@Autowired
	private KafkaTemplate<String, Object> kafkaTemplate ;
	
	private static final Logger logger = LoggerFactory.getLogger(TaskService.class);
	
	public void sendEmail(String fromEmail,String toEmail,String subject,String body) {
		
		SimpleMailMessage mailMessage=new SimpleMailMessage();
		mailMessage.setFrom(fromEmail);
		mailMessage.setTo(toEmail);
		mailMessage.setSubject(subject);
		mailMessage.setText(body);
		
		mailSender.send(mailMessage);
	}
	

	public TaskDto createTask(TaskDto taskDto) {
		// TODO Auto-generated method stub
		String userName= taskDto.getAssign();
		User user=userDao.findByUserName(userName);
		String userEmail=user.getUserEmail();
		Task task=new Task();
		task.setTaskId(taskDto.getTaskId());
		task.setTaskDescription(taskDto.getTaskDescription());
		task.setTaskNumber(taskDto.getTaskNumber());
		task.setStartingDate(taskDto.getStartingDate());
		task.setEstimateDate(taskDto.getEstimateDate());
		task.setTaskStatus(taskDto.getTaskStatus());
		task.setSprint(taskDto.getSprint());
		task.setAssign(user);

		Task savedTask=taskDao.save(task);
		TaskDto taskDto1=new TaskDto();
		taskDto1.setTaskId(task.getTaskId());
		taskDto1.setTaskDescription(task.getTaskDescription());
		taskDto1.setTaskNumber(task.getTaskNumber());
		taskDto1.setStartingDate(task.getStartingDate());
		taskDto1.setEstimateDate(task.getEstimateDate());
		taskDto1.setTaskStatus(task.getTaskStatus());
		taskDto1.setSprint(task.getSprint());
		taskDto1.setAssign(task.getAssign().getUserName());
		sendEmail("ywaran500@gmail.com", userEmail, "New Task","Task Description : "+ taskDto1.getTaskDescription()+"\n"+"Starting Date : "+taskDto1.getStartingDate()+"\n"+"Estimate Date : "+taskDto1.getEstimateDate());
		return taskDto1;
		
	}

	

	public Task find(long taskId) {
		// TODO Auto-generated method stub
		return taskDao.findById(taskId).orElse(null);
	}


	



	public List<TaskDto> getAllTask() {
		// TODO Auto-generated method stub
		TaskDto task1=new TaskDto();
		List<Task> taskList= taskDao.findAll();
		List<TaskDto> dtoList=new ArrayList<>();
		
		for (Task data : taskList) {
			TaskDto dto = new TaskDto();
			dto.setTaskId(data.getTaskId());
			dto.setTaskNumber(data.getTaskNumber());
			dto.setTaskDescription(data.getTaskDescription());
			dto.setStartingDate(data.getStartingDate());
			dto.setEstimateDate(data.getEstimateDate());
			dto.setTaskStatus(data.getTaskStatus());
			dto.setSprint(data.getSprint());
			dto.setAssign(data.getAssign().getUserName());
			
			dtoList.add(dto);
			
			}
		return dtoList;
		
	}



	public void deleteTask(Long id) {
		// TODO Auto-generated method stub
		taskDao.deleteById(id);
		
	}

	
	public void updateTask(Long id, TaskDto task) {
		// TODO Auto-generated method stub
		Task tasks = taskDao.findById(id).orElseThrow();
		tasks.setTaskDescription(task.getTaskDescription());
		tasks.setTaskNumber(task.getTaskNumber());
		
		String userName= task.getAssign();
		User user=userDao.findByUserName(userName);
		
		tasks.setAssign(user);
		tasks.setStartingDate(task.getStartingDate());
		tasks.setEstimateDate(task.getEstimateDate());
		tasks.setTaskStatus(task.getTaskStatus());
		tasks.setSprint(task.getSprint());
		taskDao.save(tasks);
		
		task.setAssign(tasks.getAssign().getUserName());
		task.setTaskDescription(tasks.getTaskDescription());
		task.setTaskNumber(tasks.getTaskNumber());
		task.setStartingDate(tasks.getStartingDate());
		task.setEstimateDate(tasks.getEstimateDate());
		task.setTaskStatus(tasks.getTaskStatus());
		task.setSprint(tasks.getSprint());
		logger.info(String.format("Message sent %s",task.toString()));
		kafkaTemplate.send("taskOne",(task));
	}
	
	@KafkaListener(topics="taskOne")
	public void consume(TaskDto message) {
		try {
			logger.info(String.format("Message received %s", message.toString()));

		}catch (Exception e) {
			// TODO: handle exception
		}
	}



	public List<TaskDto> getAllTaskByUserId(Long id) {
		// TODO Auto-generated method stub
		
		User user=userDao.findById(id).get();
		String userName=user.getUserName();
		List<Task> assign=taskDao.findByAssign_UserName(userName);


		List<Task> taskList= assign;
		List<TaskDto> dtoList=new ArrayList<>();
		for (Task data : taskList) {
			TaskDto dto = new TaskDto();
			dto.setTaskId(data.getTaskId());
			dto.setTaskNumber(data.getTaskNumber());
			dto.setTaskDescription(data.getTaskDescription());
			dto.setStartingDate(data.getStartingDate());
			dto.setEstimateDate(data.getEstimateDate());
			dto.setTaskStatus(data.getTaskStatus());
			dto.setSprint(data.getSprint());
			dto.setAssign(data.getAssign().getUserName());
			
			dtoList.add(dto);
			
			}
		return dtoList;
	}



	
	public List<TaskDto> filterTask(String key,String value){
		
		Session session=sessionFactory.openSession();
		CriteriaBuilder builder =session.getCriteriaBuilder();
		CriteriaQuery<TaskDto> criteriaquery=builder.createQuery(TaskDto.class);
		Root<Task> root=criteriaquery.from(Task.class);
		
		

		criteriaquery.multiselect( root.get("taskId"), root.get("taskDescription"), root.get("taskNumber"),
				root.get("assign").get("userName"), root.get("startingDate"), root.get("estimateDate"), root.get("taskStatus"),
				root.get("sprint"));
		if(key.equals("assign")) {
			criteriaquery.where(
					 builder.equal(root.get("assign").get("userName"), value)
			            
			        );
			
		}
		else {
			 criteriaquery.where(
					 builder.equal(root.get(key), value)
			            
			        );
			 }
		TypedQuery<TaskDto> query = session.createQuery(criteriaquery);
        
		return query.getResultList();
	}
	
	

	

}
